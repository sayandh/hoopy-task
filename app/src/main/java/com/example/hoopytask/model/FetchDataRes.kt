package com.example.hoopytask.model

import android.os.Parcel
import android.os.Parcelable

data class FetchDataRes(
    val data: List<Data>,
    val metadata: Metadata
) {
    data class Data(
        val contact: Long,
        val email: String,
        val id: Int,
        val image_url: String,
        val name: String,
        val username: String
    ):Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeLong(contact)
            parcel.writeString(email)
            parcel.writeInt(id)
            parcel.writeString(image_url)
            parcel.writeString(name)
            parcel.writeString(username)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Data> {
            override fun createFromParcel(parcel: Parcel): Data {
                return Data(parcel)
            }

            override fun newArray(size: Int): Array<Data?> {
                return arrayOfNulls(size)
            }
        }
    }

    data class Metadata(
        val response_code: Int,
        val response_text: String
    )
}