package com.example.hoopytask.model

data class UploadImageRes(
    val metadata: Metadata,
    val urls: List<String>
) {
    data class Metadata(
        val response_code: Int,
        val response_text: String
    )
}