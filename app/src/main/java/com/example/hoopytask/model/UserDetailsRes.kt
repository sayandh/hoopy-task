package com.example.hoopytask.model

data class UserDetailsRes(
    val `data`: Data,
    val metadata: Metadata
) {
    data class Data(
        val contact: String,
        val email: String,
        val id: Int,
        val image_url: String,
        val name: String,
        val username: String
    )

    data class Metadata(
        val response_code: Int,
        val response_text: String
    )
}