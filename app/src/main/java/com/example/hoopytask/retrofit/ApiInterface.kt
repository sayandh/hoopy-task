package com.example.hoopytask.retrofit


import com.example.hoopytask.model.FetchDataRes
import com.example.hoopytask.model.UploadImageRes
import com.example.hoopytask.model.UserDetailsRes
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {


    @FormUrlEncoded
    @POST("insert_test")
    fun insertData(
        @Field("email") email: String, @Field("name") name: String,
        @Field("username") username: String, @Field("contact") mobile: String,
        @Field("image_url") imageUrl: String
    ): Call<UserDetailsRes>

    @FormUrlEncoded
    @POST("update_data_test")
    fun updateData(
        @Field("email") email: String, @Field("name") name: String,
        @Field("username") username: String, @Field("contact") mobile: String,
        @Field("user_id") userId: Int
    ): Call<UserDetailsRes>

    @FormUrlEncoded
    @POST("fetch_data_test")
    fun fetchData(
        @Field("email") email: String
    ): Call<FetchDataRes>

    @Multipart
    @POST("upload_test")
    fun uploadImage( @Part image:MultipartBody.Part?): Call<UploadImageRes>


}