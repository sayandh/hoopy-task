package com.example.hoopytask.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.hoopytask.R
import com.example.hoopytask.model.FetchDataRes
import com.example.hoopytask.model.UploadImageRes
import com.example.hoopytask.model.UserDetailsRes
import com.example.hoopytask.retrofit.ApiClient
import com.example.hoopytask.utils.AlertProgress
import com.example.hoopytask.utils.SharedPreference
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.regex.Pattern

class RegisterActivity : AppCompatActivity() {

    private var email=""
    private var name=""
    private var username=""
    private var imagePath=""
    private var imageUrl=""
    private var mobile=""
    private val PERMISSION_REQUEST_CODE=10
    private val RESULT_LOAD_IMAGE=11
    private var isEdit=false
    private var userId=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var data:FetchDataRes.Data
        if (intent.hasExtra("data")){
            var data:FetchDataRes.Data=intent.getParcelableExtra("data")!!
            isEdit=true
            setData(data)
        }

        if(checkAlreadyRegistered()&&!isEdit){
            startActivity(Intent(this@RegisterActivity,HomeActivity::class.java))
            finish()
        }

        btnSubmit.setOnClickListener{
            Log.e("IsValid","==${isValid()}")
            if (isValid()){
                if (!isEdit) {
                    insertData()
                }else{
                    updateData()
                }

            }
        }
        edtUpload.setOnClickListener {
            takeImage()
        }

    }

    private fun setData(data: FetchDataRes.Data) {
        edtName.setText(data.name)
        edtUsername.setText(data.name)
        edtUpload.setText(data.image_url)
        imageUrl=data.image_url
        edtUpload.visibility= View.GONE
        edtEmail.setText(data.email)
        edttContact.setText(data.contact.toString())
        titleReg.text=getString(R.string.edit_details)
        userId=data.id
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                RESULT_LOAD_IMAGE -> {
                    imagePath = if (data!!.data != null) {
                        val selectedImage = data.data
                        getRealPathFromURI(selectedImage!!)
                    } else {
                        val photo = data.extras?.get("data") as Bitmap
                        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                        val tempUri = getImageUri(applicationContext, photo)
                        // CALL THIS METHOD TO GET THE ACTUAL PATH
                        getRealPathFromURI(tempUri)
                    }
                    var names = imagePath.split("/")
                    edtUpload.setText(names[names.size - 1])
                    uploadImage()
                }
            }
    }

    private fun checkPersmission(): Boolean {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)&& ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun takeImage() {
        if (checkPersmission()) {
            val camIntent = Intent("android.media.action.IMAGE_CAPTURE")
//            val gallIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//            gallIntent.type = "image/*"
//            val chooser = Intent.createChooser(gallIntent, "Choose an option below")
//            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(camIntent))
            startActivityForResult(camIntent, RESULT_LOAD_IMAGE)
        } else {
            requestPermission()
        }
    }

    private fun isValid(): Boolean {
        name=edtName.editableText.toString()
        email=edtEmail.editableText.toString()
        username=edtUsername.editableText.toString()
        mobile=edttContact.editableText.toString()
        val regx = "[a-zA-Z]+\\.?"
        val pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE)
        var valid = true
         if (name.length<=3) {
            edtName.error = "Invalid name"
            valid = false
        }else if (email.isEmpty() || TextUtils.isEmpty(email) && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
             edtName.error=null
            edtEmail.error = "Invalid Email"
            valid = false
        }
        else if (username.length<=3) {
            edtName.error=null
            edtUsername.error = "Invalid username"
            valid = false
        }
        else if (mobile.length!=10||!Patterns.PHONE.matcher(mobile).matches()) {
            edtName.error=null
            edttContact.error = "Invalid Mobile"
            valid = false
        }else if (imageUrl.isEmpty()) {
             edttContact.error=null
             edtUpload.error = "Please upload ur Image"
             valid = false
         }
         else{
             edtUpload.error=null
             valid=true

        }

        return valid
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            PERMISSION_REQUEST_CODE
        )
    }

    fun getRealPathFromURI(uri: Uri): String {
        var path = ""
        if (contentResolver != null) {
            val cursor = contentResolver.query(uri, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    private fun uploadImage() {
        var image: MultipartBody.Part? = null
        if (this.imagePath.isNotEmpty()) {
            val file = File(this.imagePath)
            var requestFile =
                RequestBody.create(MediaType.parse("imagePath/*"), file)
            image = MultipartBody.Part.createFormData("pan_doc", file.name, requestFile)

        }

        val call: Call<UploadImageRes> = ApiClient.getClient.uploadImage(
            image = image
        )
        val alertProgress = AlertProgress(this)
        alertProgress.showProgressDialog("Uploading Image . . ")
        call.enqueue(object : Callback<UploadImageRes> {
            override fun onFailure(call: Call<UploadImageRes>, t: Throwable) {
                alertProgress.dismissProgresDialog()
                alertProgress.showErrorDialog(
                    "Registration Failed",
                    "Please try again later"
                )
            }

            override fun onResponse(call: Call<UploadImageRes>, response: Response<UploadImageRes>) {
                alertProgress.dismissProgresDialog()
                Log.e("RESPONSE","==${response.body()}")
                if (response.isSuccessful) {

                    if (response.body()!!.metadata.response_code==200) {
                        alertProgress.showSuccessDialog(
                            "Success",response.body()?.metadata?.response_text

                        )
                        imageUrl=response.body()!!.urls[0]
                        edtUpload.setText(imageUrl)
                    }else{
                        alertProgress.showErrorDialog(
                            "Success","Upload Failed"

                        )
                    }
                } else {
                    alertProgress.showErrorDialog(
                        "Failed",
                        "Upload Failed"
                    )
                }
            }


        })
    }

    private fun insertData() {

        val call: Call<UserDetailsRes> = ApiClient.getClient.insertData(name = name,
            username = username,
            email = email,
            imageUrl = imageUrl,
            mobile = mobile

        )
        val alertProgress = AlertProgress(this)
        alertProgress.showProgressDialog("Registering User . . ")
        call.enqueue(object : Callback<UserDetailsRes> {
            override fun onFailure(call: Call<UserDetailsRes>, t: Throwable) {
                alertProgress.dismissProgresDialog()
                alertProgress.showErrorDialog(
                    "Registration Failed",
                    "Please try again later"
                )
            }

            override fun onResponse(call: Call<UserDetailsRes>, response: Response<UserDetailsRes>) {
                alertProgress.dismissProgresDialog()
                if (response.isSuccessful) {

                    if (response.body()!!.metadata.response_code==200) {
                        val sharedPreference=SharedPreference(this@RegisterActivity)
                        sharedPreference.save("email",email)
                        sharedPreference.save("isLoggedIn",true)
                        startActivity(Intent(this@RegisterActivity,HomeActivity::class.java))
                        finish()
                    }else{
                        alertProgress.showErrorDialog(
                            "Failed","Please try again later"

                        )
                    }
                } else {
                    alertProgress.showErrorDialog(
                        "Failed",
                        "Please try again later"
                    )
                }
            }


        })
    }

    private fun updateData() {

        val call: Call<UserDetailsRes> = ApiClient.getClient.updateData(name = name,
            username = username,
            email = email,
            userId = userId,
            mobile = mobile

        )
        val alertProgress = AlertProgress(this)
        alertProgress.showProgressDialog("Updating User . . ")
        call.enqueue(object : Callback<UserDetailsRes> {
            override fun onFailure(call: Call<UserDetailsRes>, t: Throwable) {
                alertProgress.dismissProgresDialog()
                alertProgress.showErrorDialog(
                    "Update Failed",
                    "Please try again later"
                )
            }

            override fun onResponse(call: Call<UserDetailsRes>, response: Response<UserDetailsRes>) {
                alertProgress.dismissProgresDialog()
                if (response.isSuccessful) {

                    if (response.body()!!.metadata.response_code==200) {
                        val sharedPreference=SharedPreference(this@RegisterActivity)
                        sharedPreference.save("email",email)
                        sharedPreference.save("isLoggedIn",true)
                        startActivity(Intent(this@RegisterActivity,HomeActivity::class.java))
                        finish()
                    }else{
                        alertProgress.showErrorDialog(
                            "Update Failed",
                            "Please try again later"
                        )
                    }
                } else {
                    alertProgress.showErrorDialog(
                        "Update Failed",
                        "Please try again later"
                    )
                }
            }


        })
    }

    private fun checkAlreadyRegistered():Boolean{
        val sharedPreference=SharedPreference(this)
        return sharedPreference.getValueBoolien("isLoggedIn",false);
    }

}



