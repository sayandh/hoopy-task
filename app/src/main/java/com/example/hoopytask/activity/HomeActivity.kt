package com.example.hoopytask.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.hoopytask.R
import com.example.hoopytask.model.FetchDataRes
import com.example.hoopytask.retrofit.ApiClient
import com.example.hoopytask.utils.AlertProgress
import com.example.hoopytask.utils.SharedPreference
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivity : AppCompatActivity(){
    private var email=""
    private  lateinit var sharedPreference: SharedPreference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        sharedPreference= SharedPreference(this)
        email=sharedPreference.getValueString("email")!!
        fetchData()


    }

    private fun fetchData() {

        val call: Call<FetchDataRes> = ApiClient.getClient.fetchData(email = email

        )
        val alertProgress = AlertProgress(this)
        alertProgress.showProgressDialog("Fetching Details")
        call.enqueue(object : Callback<FetchDataRes> {
            override fun onFailure(call: Call<FetchDataRes>, t: Throwable) {
                alertProgress.dismissProgresDialog()
                alertProgress.showErrorDialog(
                    "Something Went Wrong",
                    "Please try again later"
                )
            }

            override fun onResponse(call: Call<FetchDataRes>, response: Response<FetchDataRes>) {
                alertProgress.dismissProgresDialog()
                if (response.isSuccessful) {

                    if (response.body()!=null&&response.body()!!.metadata.response_code==200) {
                        textName.text=response.body()!!.data[0].name
                        textEmail.text=response.body()!!.data[0].email
                        textContact.text=response.body()!!.data[0].contact.toString()
                        textUserName.text=response.body()!!.data[0].username
                        Glide.with(this@HomeActivity).load(response.body()!!.data[0].image_url).into(imageView)
                        btnEdit.setOnClickListener {
                            val intent=Intent(this@HomeActivity,RegisterActivity::class.java)
                            intent.putExtra("data",response.body()!!.data[0])
                            startActivity(intent)
                        }
                    }else{
                        alertProgress.showErrorDialog(
                            "Something Went Wrong",
                            "Please try again later"
                        )
                    }
                } else {
                    alertProgress.showErrorDialog(
                        "Something Went Wrong",
                        "Please try again later"
                    )
                }
            }


        })
    }
}