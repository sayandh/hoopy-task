package com.example.hoopytask.utils

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.hoopytask.R

class AlertProgress(val context: Context) {
    private var alertDialog: AlertDialog? = null

    private lateinit var txtTitle:TextView
    private lateinit var txtDescription:TextView
    private lateinit var imageView:ImageView
    private lateinit var btnClose:Button

    fun showProgressDialog(message:String) {
        initializeAlert(context)
        txtTitle.visibility=View.GONE
        txtDescription.visibility=View.VISIBLE
        imageView.visibility=View.VISIBLE
        btnClose.visibility=View.GONE

        txtDescription.text=message
        Glide.with(context).asGif().load(R.drawable.loading).into(imageView)
    }

    fun dismissProgresDialog() {
        try {
            alertDialog!!.dismiss()
        }catch (e:KotlinNullPointerException){

        }

    }

    fun showSuccessDialog(title: String,description: String?) {
        initializeAlert(context)
        txtTitle.visibility=View.VISIBLE
        txtDescription.visibility=View.VISIBLE
        imageView.visibility=View.VISIBLE
        btnClose.visibility=View.VISIBLE

        txtTitle.text=title
        txtDescription.text="$description"
        Glide.with(context).asGif().load(R.drawable.success).into(imageView)
    }
    fun showSuccessDialog(title: String,description: String,clickListener:View.OnClickListener) {
        initializeAlert(context)
        txtTitle.visibility=View.VISIBLE
        txtDescription.visibility=View.VISIBLE
        imageView.visibility=View.VISIBLE
        btnClose.visibility=View.VISIBLE

        btnClose.setOnClickListener(clickListener)

        txtTitle.text=title
        txtDescription.text=description
        Glide.with(context).asGif().load(R.drawable.success).into(imageView)
    }

//    fun showPendingDialog(title: String,description: String,clickListener:View.OnClickListener) {
//        initializeAlert(context)
//        txtTitle.visibility=View.VISIBLE
//        txtDescription.visibility=View.VISIBLE
//        imageView.visibility=View.VISIBLE
//        btnClose.visibility=View.VISIBLE
//
//        btnClose.setOnClickListener(clickListener)
//
//        txtTitle.text=title
//        txtDescription.text=description
//        Glide.with(context).load(R.drawable.icon_pending).into(imageView)
//    }
    fun showErrorDialog(title: String,description: String) {
        initializeAlert(context)
        txtTitle.visibility=View.VISIBLE
        txtDescription.visibility=View.VISIBLE
        imageView.visibility=View.VISIBLE
        btnClose.visibility=View.VISIBLE

        txtTitle.text=title
        txtTitle.setTextColor(context.resources.getColor(R.color.red) )
        txtDescription.text=description
        Glide.with(context).load(R.drawable.error).into(imageView)
    }

    fun showErrorDialog(title: String,description: String,clickListener: View.OnClickListener) {
        initializeAlert(context)
        txtTitle.visibility=View.VISIBLE
        txtDescription.visibility=View.VISIBLE
        imageView.visibility=View.VISIBLE
        btnClose.visibility=View.VISIBLE

        btnClose.setOnClickListener(clickListener)
        txtTitle.text=title
        txtTitle.setTextColor(context.resources.getColor(R.color.red) )
        txtDescription.text=description
        Glide.with(context).load(R.drawable.error).into(imageView)
    }

    private fun initializeAlert(context: Context) {
        val dialogBuilder = AlertDialog.Builder(context)
        alertDialog = dialogBuilder.create()
        alertDialog!!.show()


        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.aprogress_dialog, null)
        alertDialog!!.window!!.setContentView(dialogView)
        alertDialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation_2
        alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        alertDialog!!.setCancelable(false)
        txtTitle=dialogView.findViewById(R.id.text_title)
        txtDescription=dialogView.findViewById(R.id.text_dialog)
        imageView=dialogView.findViewById(R.id.success_image)
        btnClose=dialogView.findViewById(R.id.btn_close)

        btnClose.setOnClickListener{alertDialog!!.dismiss()}
    }


}